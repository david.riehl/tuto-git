# Tuto Git

Histoire de découvrir git ! :D

## Commandes vues

- git init
- git status
- git add 
- git commit -m "initial commit"
- git log
- git config --global user.name "David RIEHL"
- git config --global user.email "me@domain.com"
- git branch nomDeLaBranche
- git checkout nomDeLaBranche
- git checkout -b nomDeLaBranche
- git stash
- git stash list
- git stash show
- git stash pop
- git stash apply
- git remote add origin git@gitlab.com:david.riehl/tuto-git.git
- git merge --no-ff

## Commit

- user.name
- user.email
- datetime
- message
- hash
